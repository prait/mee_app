<?php

namespace Theme\Nest\Http\Resources;

use EcommerceHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use RvMedia;

class ProductMiniResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $defaultImage = RvMedia::getDefaultImage();
        $originalProduct = !$this->is_variation ? $this : $this->original_product;
        $data = [
            'id'            => $originalProduct->id,
            'name'          => $originalProduct->name,
            'url'           => $originalProduct->url,
            'image'         => RvMedia::getImageUrl($originalProduct->image, 'product-thumb', false, $defaultImage),
            'price'         => format_price($originalProduct->front_sale_price_with_taxes),
            'sale_price'    => $originalProduct->front_sale_price !== $originalProduct->price ? format_price($originalProduct->front_sale_price) : null,
        ];

        if (EcommerceHelper::isReviewEnabled() && $originalProduct->reviews_count) {
            $data = array_merge($data, [
                'reviews_avg'   => $originalProduct->reviews_avg,
                'reviews_count' => $originalProduct->reviews_count
            ]);
        }

        return $data;
    }
}
